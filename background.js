/**
 * Background script for MTNL Popup sniffing.
 * @author me@nishantarora.in (Nishant Arora)
 */

// Namespace MTNL_unEvil.
MTNL_unEvil = {};

// List of evil servers.
MTNL_unEvil.mtnlEvilServers = ['adphonso.com', '203.94.243.40'];

// Filter for saviour function.
MTNL_unEvil.saviourFliters = { urls: ["<all_urls>"] };

// Specs for saviour functions.
MTNL_unEvil.extraInfoSpec = [ "blocking" ];

// Config for reload.
MTNL_unEvil.reloadConfig = { bypassCache: true };

/**
 * Saviour function
 * @param {object} request
 */
MTNL_unEvil.saviourCallBack = function(request) {
    for (var server in MTNL_unEvil.mtnlEvilServers) {
        if (request.url.indexOf(MTNL_unEvil.mtnlEvilServers[server]) > -1) {
            // Reloading the tab.
            if (request.tabId != -1) {
                chrome.tabs.reload(request.tabId, MTNL_unEvil.reloadConfig);
            }
        }
    }
};

/**
 * Blocking requests to MTNL popup servers.
 */
//chrome.webRequest.onBeforeRequest.addListener(
chrome.webRequest.onBeforeRequest.addListener(
    // Checking if it is not redirecting to all steps.
    MTNL_unEvil.saviourCallBack,
    // Applies to following url patterns
    MTNL_unEvil.saviourFliters,
    // In request blocking mode
    MTNL_unEvil.extraInfoSpec
);